CC = gcc
CFLAGS = -std=c17 -pedantic -Wall -Wextra -g

poblacion: poblacion.c
	$(CC) $(CFLAGS) -o poblacion poblacion.c

clean:
	rm -v poblacion
