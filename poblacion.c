#include <stdio.h>
#include <string.h>
#define CANT_PAIS 3
#define MAX_PROV 3

typedef struct {
	char nombre[20];
	unsigned poblacion;
	float porcentaje;
} Provincia;

typedef struct {
	char nombre[20];
	unsigned provincias;
	unsigned pobTot;
} Pais;

typedef struct{
	char nombre[20];
	char nPais[20];
	float porcentaje;
} MaxProv;

//funciones
MaxProv poblacion(Provincia provs[][MAX_PROV], Pais paises[]);

int main(void) {
	// Datos de ejemplo
	Provincia provs[CANT_PAIS][MAX_PROV] = {
		{{"Baviera", 10562230, 0},
		{"Berlin", 9963250, 0},
		{"Bremen", 3152000, 0}},
		{{"Bretagne", 1230120, 0},
		{"Normandie", 896580, 0},
		{"Anjou", 169850, 0}},
		{{"Berna", 2356850, 0},
		{"Lucerna", 1689600, 0},
		{"Uri", 741500, 0}},
	};

	// Datos de ejemplo
	Pais paises[CANT_PAIS] = {
		{"Alemania", 3, 0},
		{"Francia", 3, 0},
		{"Suize", 3, 0},
	};

       	MaxProv mProv = poblacion (provs, paises);

	// Imprimo información por Pais/Provincia
	for (size_t i = 0; i < CANT_PAIS; i++) {
		printf("\nPaís: %s\n", paises[i].nombre);
		printf("Polación Total: %u\n", paises[i].pobTot);
		puts("Provincias:");

		for (size_t j = 0; j < paises[i].provincias; j++) { 
			printf("\n\tNombre: %s\n", provs[i][j].nombre);
			printf("\tPoblación: %u\n", provs[i][j].poblacion);
			printf("\tPorcentaje: %.2f\n", provs[i][j].porcentaje);
		}
	}

	puts("\nProvincia con mayor población con respecto a su país:");
	printf("Provincia: %s\n", mProv.nombre);
	printf("Pais: %s\n", mProv.nPais);
	printf("Porcentaje: %.2f\n", mProv.porcentaje);

	return 0;
}


MaxProv poblacion(Provincia provs[][MAX_PROV], Pais paises[])
{
	for (size_t i = 0; i < CANT_PAIS; i++) {
		paises[i].pobTot = 0;

		for (size_t j = 0; j < paises[i].provincias; j++) { 
			paises[i].pobTot +=provs[i][j].poblacion;
		}
	}

	int iPais, jProv;
	float maxPorc=0;

	for (size_t i = 0; i < CANT_PAIS; i++) {
		for (size_t j = 0; j < paises[i].provincias; j++) {
			provs[i][j].porcentaje = ((float)provs[i][j].poblacion /
					(float)paises[i].pobTot) * 100.0;

				if (provs[i][j].porcentaje > maxPorc) {
					maxPorc = provs[i][j].porcentaje;
					iPais = i;
					jProv = j;
				}
		}
	}

	MaxProv maximo;
	
	strcpy(maximo.nombre, provs[iPais][jProv].nombre);
	strcpy(maximo.nPais, paises[iPais].nombre);
	maximo.porcentaje = maxPorc;

	return maximo;
}
