# Población

Teniendo en cuenta el crecimiento poblacional de Europa, se está haciendo un
análisis demográfico de los diferentes países y cada una de las
provincias/regiones dentro de ellos. La información del continente está
almacenada en una matriz de la siguiente manera, donde en cada fila está la
información de las provincias de un país; cada país puede tener una cantidad
diferente de provincias. Para cada provincia se conoce su nombre y su población
y se quiere determinar el % de población con respecto a la población del país.

```text
Baviera		Berlin		Bremen		...
10562230	9963250		3152000 	...
% 0 		% 0		% 0		...

Bretagne	Normandie	Anjou		...
1230120		896580		169850		...
% 0		% 0		% 0		...

...		...		...		...

Berna		Lucerna		Uri
2356850		1689600		741500
% 0		% 0		% 0
```

Por otra parte, se tiene la información básica del país. Inicialmente se conoce
el nombre y la cantidad de provincias. Se quiere calcular la población total.

```text
Alemania	Francia		...	Suiza
16		18		...	26
0		0		...	0
```

1. Defina claramente todos los arreglos/matrices y estructuras necesarias para
   modelar el continente y los países.

1. Construya la función población que recibe la información inicial de las
   provincias y los países y actualiza la información del continente. Con base
   en la información de población de cada provincia, debe calcular el total de
   población de cada país y el % que representa la población de cada provincia
   con respecto a la población del país. Actualice los datos de acuerdo con los
   resultados. La función además debe encontrar la provincia que tiene el % más
   alto poblacional con respecto a su país y debe retornar: el nombre de esa
   provincia, el nombre del país correspondiente y el valor de dicho
   porcentaje. Tenga en cuenta como agrupar la información para poder
   retornarla.
